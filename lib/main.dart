import 'package:flutter/material.dart';
//This mini application displays ActivEdge Technologies brand image with
//image also as logo.(Long life and more wins to the firm)

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          body: Center(
            child: Image(
              image: AssetImage('images/activedge.png'),
            ),
          ),
          appBar: AppBar(
            title: Text('I am a Family of AET(ActiveEdge Technologies)'),
            backgroundColor: Colors.deepOrange,
          ),
          backgroundColor: Colors.grey,
        ),
      ),
    );
